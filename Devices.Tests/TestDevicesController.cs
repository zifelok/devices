﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Devices.Services;
using Devices.Web.Controllers;
using AutoMapper;
using Devices.Web.App_Start;
using System.Net.Http;
using System.Web.Http;
using Devices.Web.Models.Devices;
using Devices.Data.Entities;
using System.Linq;
using System.Collections.Generic;

namespace Devices.Tests
{
    [TestClass]
    public class TestDevicesController
    {
        private static List<Device> devices = new List<Device>()
            {
                new Device()
                {
                    Id = Guid.NewGuid(),
                    Name = "Name1",
                    SecretKey = "SecretKey1",
                    Status = DeviceStatus.New,
                    StatusModificationDate = DateTime.Now
                },
                new Device()
                {
                    Id = Guid.NewGuid(),
                    Name = "Name2",
                    SecretKey = "SecretKey2",
                    Status = DeviceStatus.Ok,
                    StatusModificationDate = DateTime.Now
                },
                new Device()
                {
                    Id = Guid.NewGuid(),
                    Name = "Name3",
                    SecretKey = "SecretKey3",
                    Status = DeviceStatus.Stale,
                    StatusModificationDate = DateTime.Now
                },
                new Device()
                {
                    Id = Guid.NewGuid(),
                    Name = "Name4",
                    SecretKey = "SecretKey4",
                    Status = DeviceStatus.Unhealthy,
                    StatusModificationDate = DateTime.Now
                },
            };

        [TestMethod]
        public void GetAll_ShouldReturnAllDevices()
        {
            // Arrange
            var mock = new Mock<IDeviceService>();
            mock.Setup(s => s.GetAll()).Returns(devices);
            var controller = GetController(mock);

            // Act
            var response = controller.Get();

            // Assert
            IEnumerable<DeviceListModel> result;
            Assert.AreEqual(System.Net.HttpStatusCode.OK, response.StatusCode);
            Assert.IsTrue(response.TryGetContentValue(out result));
            Assert.AreEqual(4, result.Count());
        }

        [TestMethod]
        public void GetAllByStatusOk_ShouldReturnDevicesWithCorrectStatus()
        {
            // Arrange
            var mock = new Mock<IDeviceService>();
            mock.Setup(s => s.GetByStatus(DeviceStatus.Ok)).Returns(devices.Where(d => d.Status == DeviceStatus.Ok).ToList());
            var controller = GetController(mock);

            // Act
            var response = controller.Get(DeviceStatusModel.Ok);

            // Assert
            IEnumerable<DeviceListModel> result;
            Assert.AreEqual(System.Net.HttpStatusCode.OK, response.StatusCode);
            Assert.IsTrue(response.TryGetContentValue(out result));
            Assert.AreEqual(1, result.Count());
            Assert.IsTrue(result.All(d => d.Status == DeviceStatusModel.Ok));
        }

        [TestMethod]
        public void GetAllById_ShouldReturnDeviceWithCorrectId()
        {
            // Arrange
            var mock = new Mock<IDeviceService>();
            mock.Setup(s => s.Get(devices[0].Id)).Returns(devices[0]);
            var controller = GetController(mock);

            // Act
            var response = controller.GetDevice(devices[0].Id);

            // Assert
            DeviceModel device;
            Assert.AreEqual(System.Net.HttpStatusCode.OK, response.StatusCode);
            Assert.IsTrue(response.TryGetContentValue(out device));
            Assert.AreEqual(devices[0].Id, device.Id);
        }

        [TestMethod]
        public void GetAllById_ShouldReturnNotFound()
        {
            // Arrange
            var guid = Guid.NewGuid();
            var mock = new Mock<IDeviceService>();
            mock.Setup(s => s.Get(guid)).Returns<Device>(null);
            var controller = GetController(mock);

            // Act
            var response = controller.GetDevice(Guid.NewGuid());

            // Assert
            Assert.AreEqual(System.Net.HttpStatusCode.NotFound, response.StatusCode);
        }

        [TestMethod]
        public void Post_ShouldReturnCreated()
        {
            // Arrange
            var mock = new Mock<IDeviceService>();
            var device = devices[0];
            mock.Setup(s => s.Add(It.Is<Device>(d => true))).Returns(true);
            mock.Setup(s => s.Get(It.Is<Guid>(d => true))).Returns(device);
            var controller = GetController(mock);

            // Act
            var response = controller.Post(new CreateDeviceModel()
            {
                Name = device.Name,
                SecretKey = device.SecretKey
            });

            // Assert
            DeviceModel result;
            Assert.AreEqual(System.Net.HttpStatusCode.Created, response.StatusCode);
            Assert.IsTrue(response.TryGetContentValue(out result));
            Assert.AreEqual(device.Name, result.Name);
            Assert.AreEqual(device.SecretKey, result.SecretKey);
            Assert.AreEqual(DeviceStatusModel.New, result.Status);
        }

        [TestMethod]
        public void UpdateStaus_ShouldReturnOk()
        {
            // Arrange
            var mock = new Mock<IDeviceService>();
            var device = devices[0];
            mock.Setup(s => s.UserSetDeviceStatus(device.Id, DeviceStatus.Ok, device.SecretKey)).Returns(true);
            mock.Setup(s => s.Get(It.Is<Guid>(d => true))).Returns(device);
            var controller = GetController(mock);

            // Act
            var response = controller.UpdateStaus(device.Id, new DeviceModel() { Status = DeviceStatusModel.Ok, SecretKey = device.SecretKey });

            // Assert
            Assert.AreEqual(System.Net.HttpStatusCode.OK, response.StatusCode);
        }

        [TestMethod]
        public void UpdateStaus_ShouldReturnNotModified()
        {
            // Arrange
            var mock = new Mock<IDeviceService>();
            var device = devices[0];
            mock.Setup(s => s.UserSetDeviceStatus(device.Id, DeviceStatus.Ok, device.SecretKey)).Returns(false);
            mock.Setup(s => s.Get(It.Is<Guid>(d => true))).Returns(device);
            var controller = GetController(mock);

            // Act
            var response = controller.UpdateStaus(device.Id, new DeviceModel() { Status = DeviceStatusModel.Ok, SecretKey = device.SecretKey });

            // Assert
            Assert.AreEqual(System.Net.HttpStatusCode.NotModified, response.StatusCode);
        }

        private DevicesController GetController(Mock<IDeviceService> mockService)
        {
            var mapper = GetMapper();
            var controller = new DevicesController(mockService.Object, mapper);
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            return controller;
        }

        private IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfile());
            });

            return config.CreateMapper();
        }
    }
}

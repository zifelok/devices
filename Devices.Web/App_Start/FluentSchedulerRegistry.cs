﻿using Devices.Web.Jobs;
using FluentScheduler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Devices.Web.App_Start
{
    public class FluentSchedulerRegistry: Registry
    {
        public FluentSchedulerRegistry()
        {
            Schedule<UpdateDeviceStatusJob>().ToRunNow().AndEvery(30).Seconds();
        }
    }
}
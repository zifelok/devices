﻿using AutoMapper;
using Devices.Data.Entities;
using Devices.Web.Models.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Devices.Web.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Device, DeviceModel>();
            CreateMap<Device, DeviceListModel>();
            CreateMap<CreateDeviceModel, Device>();
        }
    }
}
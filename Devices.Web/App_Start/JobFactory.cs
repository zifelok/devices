﻿using FluentScheduler;
using System.Web.Mvc;
using System.Web.Http.Dependencies;

namespace Devices.Web.App_Start
{
    public class JobFactory : IJobFactory
    {
        private System.Web.Http.Dependencies.IDependencyResolver dependencyResolver;

        public JobFactory(System.Web.Http.Dependencies.IDependencyResolver dependencyResolver)
        {
            this.dependencyResolver = dependencyResolver;
        }

        public IJob GetJobInstance<T>() where T : IJob
        {
            return (T)dependencyResolver.GetService(typeof(T));
        }
    }
}
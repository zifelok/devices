﻿using AutoMapper;
using Devices.Data;
using Devices.Data.Entities;
using Devices.Services;
using Devices.Web.Models.Devices;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Devices.Web.Controllers
{
    public class DevicesController : ApiController
    {
        private readonly IDeviceService deviceService;
        private readonly IMapper mapper;

        public DevicesController(IDeviceService deviceService, IMapper mapper)
        {
            this.deviceService = deviceService;
            this.mapper = mapper;
        }

        public HttpResponseMessage Get()
        {
            try
            {
                var model = mapper.Map<IEnumerable<DeviceListModel>>(deviceService.GetAll());
                return Request.CreateResponse(HttpStatusCode.OK, model);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        public HttpResponseMessage Get(DeviceStatusModel status)
        {
            try
            {
                var model = mapper.Map<IEnumerable<DeviceListModel>>(deviceService.GetByStatus((DeviceStatus)status));
                return Request.CreateResponse(HttpStatusCode.OK, model);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        public HttpResponseMessage GetDevice(Guid id)
        {
            try
            {
                var device = deviceService.Get(id);
                if (device != null)
                {
                    var model = mapper.Map<DeviceModel>(device);
                    return Request.CreateResponse(HttpStatusCode.OK, model);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        public HttpResponseMessage Post([FromBody] CreateDeviceModel deviceModel)
        {
            try
            {
                if (deviceModel == null) Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Could not read model from body");

                var entity = mapper.Map<Device>(deviceModel);

                if (deviceService.Add(entity))
                {
                    var model = mapper.Map<DeviceModel>(entity);
                    return Request.CreateResponse(HttpStatusCode.Created, model);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Could not add device");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPatch]
        public HttpResponseMessage UpdateStaus(Guid id, [FromBody] DeviceModel deviceModel)
        {
            try
            {
                if (deviceModel == null) Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Could not read model from body");

                if (deviceService.UserSetDeviceStatus(id, (DeviceStatus)deviceModel.Status, deviceModel.SecretKey))
                {
                    var device = deviceService.Get(id);
                    var model = mapper.Map<DeviceModel>(device);
                    return Request.CreateResponse(HttpStatusCode.OK, model);
                }

                return Request.CreateResponse(HttpStatusCode.NotModified);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}

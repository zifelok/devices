﻿using Autofac;
using Autofac.Integration.WebApi;
using AutoMapper;
using Devices.Data;
using Devices.Data.Repositories;
using Devices.Services;
using Devices.Web.App_Start;
using Devices.Web.Jobs;
using FluentScheduler;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;

namespace Devices.Web
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AutoMapperConfig.ConfigureAutoMapper();
            ConfigreAutofac();
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            
            JobManager.Initialize(new FluentSchedulerRegistry());
        }

        protected void ConfigreAutofac()
        {
            var builder = new ContainerBuilder();

            // Get your HttpConfiguration.
            var config = GlobalConfiguration.Configuration;

            // Register your Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            // OPTIONAL: Register the Autofac filter provider.
            builder.RegisterWebApiFilterProvider(config);

            // OPTIONAL: Register the Autofac model binder provider.
            builder.RegisterWebApiModelBinderProvider();

            builder.RegisterInstance(AutoMapperConfig.Mapper)
                .As<IMapper>()
                .SingleInstance();
            builder.RegisterType<DeviceRepository>().As<IDeviceRepository>();
            builder.RegisterType<DeviceService>().As<IDeviceService>();
            builder.RegisterType<UpdateDeviceStatusJob>().As<UpdateDeviceStatusJob>();

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            JobManager.JobFactory = new JobFactory(config.DependencyResolver);
        }
    }
}

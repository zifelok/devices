﻿using Devices.Services;
using FluentScheduler;

namespace Devices.Web.Jobs
{
    public class UpdateDeviceStatusJob : IJob
    {
        private readonly IDeviceService deviceService;

        public UpdateDeviceStatusJob(IDeviceService deviceService)
        {
            this.deviceService = deviceService;
        }
        public void Execute()
        {
            deviceService.AutoDeviceStatusUpdate();
        }
    }
}
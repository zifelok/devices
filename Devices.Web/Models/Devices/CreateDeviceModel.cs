﻿namespace Devices.Web.Models.Devices
{
    public class CreateDeviceModel
    {
        public string Name { get; set; }
        public string SecretKey { get; set; }
    }
}
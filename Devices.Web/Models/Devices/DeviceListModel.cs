﻿using System;

namespace Devices.Web.Models.Devices
{
    public class DeviceListModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DeviceStatusModel Status { get; set; }
    }
}
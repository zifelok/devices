﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace Devices.Web.Models.Devices
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum DeviceStatusModel
    {
        [EnumMember(Value = "NEW")]
        New,
        [EnumMember(Value = "OK")]
        Ok,
        [EnumMember(Value = "UNHEALTHY")]
        Unhealthy,
        [EnumMember(Value = "STALE")]
        Stale
    }
}
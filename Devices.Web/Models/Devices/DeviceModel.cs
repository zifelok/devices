﻿using System;

namespace Devices.Web.Models.Devices
{
    public class DeviceModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string SecretKey { get; set; }
        public DeviceStatusModel Status { get; set; }
    }
}
﻿using Devices.Data.Entities;
using System;
using System.Collections.Generic;

namespace Devices.Services
{
    public interface IDeviceService
    {
        IReadOnlyCollection<Device> GetAll();
        Device Get(Guid id);
        bool Add(Device device);
        bool UserSetDeviceStatus(Guid id, DeviceStatus status, string secretKey);
        void AutoDeviceStatusUpdate();
        IReadOnlyCollection<Device> GetByStatus(DeviceStatus status);
    }
}

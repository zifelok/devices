﻿using Devices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Devices.Data.Entities;

namespace Devices.Services
{
    public class DeviceService : IDeviceService
    {
        private readonly IDeviceRepository deviceRepository;

        public DeviceService(IDeviceRepository deviceRepository)
        {
            this.deviceRepository = deviceRepository;
        }

        public bool Add(Device device)
        {
            return deviceRepository.Add(device);
        }

        public void AutoDeviceStatusUpdate()
        {
            var time = DateTime.Now.AddMinutes(-5);
            var oldDevices = deviceRepository.GetByStatus(DeviceStatus.Ok)
                .Where(d => d.StatusModificationDate <= time);
            foreach(var d in oldDevices)
            {
                deviceRepository.SetDeviceStatus(d.Id, DeviceStatus.Stale);
            }
        }

        public Device Get(Guid id)
        {
            return deviceRepository.Get(id);
        }

        public IReadOnlyCollection<Device> GetAll()
        {
            return deviceRepository.GetAll();
        }

        public IReadOnlyCollection<Device> GetByStatus(DeviceStatus status)
        {
            return deviceRepository.GetByStatus(status);
        }

        public bool UserSetDeviceStatus(Guid id, DeviceStatus status, string secretKey)
        {
            var device = deviceRepository.Get(id);
            if (device != null && device.SecretKey == secretKey && (status == DeviceStatus.Unhealthy || status == DeviceStatus.Ok))
            {
                return deviceRepository.SetDeviceStatus(id, status);
            }
            return false;
        }
    }
}

﻿namespace Devices.Data.Entities
{
    public enum DeviceStatus
    {
        New,
        Ok,
        Unhealthy,
        Stale
    }
}

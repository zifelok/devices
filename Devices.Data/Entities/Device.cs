﻿using System;

namespace Devices.Data.Entities
{
    public class Device
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string SecretKey { get; set; }
        public DeviceStatus Status { get; set; }
        public DateTime StatusModificationDate { get; set; }
    }
}

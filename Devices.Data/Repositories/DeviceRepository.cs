﻿using Devices.Data.Entities;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace Devices.Data.Repositories
{
    public class DeviceRepository : IDeviceRepository
    {
        //Restarting application should cause loss of data, there should be no persistence layer(EF, Dapper etc.)
        //It should correctly handle concurrent access by multiple clients.
        private static ConcurrentDictionary<Guid, Device> devices { get; set; } = new ConcurrentDictionary<Guid, Device>();

        public DeviceRepository()
        {
        }

        public Device Get(Guid id)
        {
            Device device = null;
            devices.TryGetValue(id, out device);
            return device;
        }

        public IReadOnlyCollection<Device> GetAll()
        {
            return devices.Values.ToList();
        }

        public bool Add(Device device)
        {
            device.Id = Guid.NewGuid();
            device.StatusModificationDate = DateTime.Now;
            return devices.TryAdd(device.Id, device);
        }

        public bool SetDeviceStatus(Guid id, DeviceStatus status)
        {
            var device = Get(id);
            if (device != null)
            {
                var newDevice = new Device()
                {
                    Id = device.Id,
                    Name = device.Name,
                    SecretKey = device.SecretKey,
                    Status = status,
                    StatusModificationDate = DateTime.Now
                };
                Func<Guid, Device, Device> factory = (k, d) => newDevice;
                devices.AddOrUpdate(id, newDevice, factory);
                return true;
            }
            return false;
        }


        public IReadOnlyCollection<Device> GetByStatus(DeviceStatus status)
        {
            return devices.Values.Where(d => d.Status == status).ToList();
        }
    }
}

﻿using Devices.Data.Entities;
using System;
using System.Collections.Generic;

namespace Devices.Data
{
    public interface IDeviceRepository
    {
        IReadOnlyCollection<Device> GetAll();
        Device Get(Guid id);
        bool Add(Device device);
        bool SetDeviceStatus(Guid id, DeviceStatus status);
        IReadOnlyCollection<Device> GetByStatus(DeviceStatus status);
    }
}
